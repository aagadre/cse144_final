import tensorflow as tf
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten, Conv2D, MaxPooling2D, Dropout, BatchNormalization
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import LearningRateScheduler
import numpy as np
from tensorflow.keras.applications import VGG16
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model
from tensorflow.keras import regularizers
from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import pandas as pd
import logging
from tensorflow.keras.applications import EfficientNetB0
import os
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.vgg16 import preprocess_input
logging.getLogger('tensorflow').setLevel(logging.ERROR)  # Suppress TensorFlow logging below ERROR level
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # Suppress TensorFlow C++ logging
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)  # Suppress deprecated function warnings

def lr_schedule(epoch):
    """Learning Rate Schedule
    Learning rate is scheduled to be reduced after 80, 120, 160, 180 epochs.
    Called automatically every epoch as part of callbacks during training.
    """
    lr = 1e-3
    if epoch > 180:
        lr *= 0.5e-3
    elif epoch > 160:
        lr *= 1e-3
    elif epoch > 120:
        lr *= 1e-2
    elif epoch > 80:
        lr *= 1e-1
    return lr



train_dir = r'C:\Users\Arin Gadre\OneDrive\Documents\CSE144\cse144_final\final_project\ucsc-cse-144-Winter-2024-final-project\train\train'
#test_dir = r'C:\Users\Arin Gadre\OneDrive\Documents\CSE144\cse144_final\final_project\ucsc-cse-144-Winter-2024-final-project\test'

vm_train_dir_arin = r'/home/aagadre/cse144_final/final_project/ucsc-cse-144-Winter-2024-final-project/train/train'
vm_test_dir_arin = r'/home/aagadre/cse144_final/final_project/ucsc-cse-144-Winter-2024-final-project/test'



# Initialize your data generator with necessary preprocessing
train_datagen = ImageDataGenerator(
    rescale=1./255,  # Rescale the pixel values to [0, 1] to aid CNN convergence
    rotation_range=90,  # Random rotation between 0 and 20 degrees
    width_shift_range=0.2,  # Random horizontal shift
    height_shift_range=0.2,  # Random vertical shift
    shear_range=0.5,  # Shear angle in counter-clockwise direction
    zoom_range=0.5,  # Random zoom
    horizontal_flip=True,  # Randomly flip inputs horizontally
    fill_mode='nearest'  # Strategy used for filling in newly created pixels
)

# This function automatically associates images with labels based on subdirectory names
# Now, use this augmented train_datagen to create a train_generator
train_generator = train_datagen.flow_from_directory(
    vm_train_dir_arin,
    target_size=(224, 224),  # Ensure images are resized to 224x224 to match the VGG16 input size
    batch_size=64,
    class_mode='categorical'  # Since you have 100 classes
)

filenames = train_generator.filenames
#print('train gen filenames: ', filenames)

mobilenet_base = MobileNetV2(weights='imagenet', include_top=False, input_tensor=Input(shape=(224, 224, 3)))

# Freeze the layers of the MobileNetV2 model to prevent them from being updated during the first training process
for layer in mobilenet_base.layers:
    layer.trainable = False

model = Sequential([
    mobilenet_base,
    
    Flatten(),
    # Adjust the fully connected layer units as needed, considering the model complexity and dataset size
    Dense(256, activation='relu', kernel_regularizer=regularizers.l2(0.001)),
    Dropout(0.5),  # Dropout to prevent overfitting
    Dense(128, activation='relu', kernel_regularizer=regularizers.l2(0.001)),
    Dropout(0.5),
    # Final layer for 100 classes
    Dense(100, activation='softmax')  # Adjusted for 100 classes
])

# Ensure to compile your model
model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['accuracy'])


# Train the model
history = model.fit(
    train_generator,
    epochs=10,  # Adjust the number of epochs based on your needs
    callbacks=[LearningRateScheduler(lr_schedule)],  # Apply the learning rate schedule
)



# Initialize your test data generator with necessary preprocessing
test_datagen = ImageDataGenerator(rescale=1./255)

# Assuming 'test_dir' is the path to your directory containing the unlabeled test images
test_generator = test_datagen.flow_from_directory(
    directory=vm_test_dir_arin,  # This should point to the parent directory of your test images
    classes=['test'],  # Assuming your images are in a subdirectory named 'test' within 'test_dir'
    target_size=(224, 224),  # Match the input size of your model
    batch_size=32,  # Adjust based on your system
    class_mode=None,  # Indicates no labels
    shuffle=False  # Keep in original order
)

# Make predictions
predictions = model.predict(test_generator, steps=int(np.ceil(test_generator.samples / test_generator.batch_size)))

# If you want, print or inspect predictions here. For example, to get the index of the highest probability class:
predicted_classes = np.argmax(predictions, axis=1)
#print(predicted_classes)

predicted_classes_indices = np.argmax(predictions, axis=1)

filenames = test_generator.filenames


file_nums = [int(f.split('/')[-1].split('.')[0]) for f in filenames]  # Adjust split as per your directory structure

# Create a DataFrame
submission_df = pd.DataFrame({
    'ID': filenames,
    'Label': predicted_classes_indices,
    'FileNum': file_nums  # Temporary column to help with sorting
})

# Sort the DataFrame by the numerical part extracted from the filenames
submission_df.sort_values(by='FileNum', inplace=True)

# Drop the temporary sorting column and reset index
submission_df.drop('FileNum', axis=1, inplace=True)
submission_df.reset_index(drop=True, inplace=True)

# Adjust filenames to remove any directory paths if present
submission_df['ID'] = submission_df['ID'].apply(lambda x: x.split('/')[-1])  # Keep only the filename
print('dataframe first 20 rows: ', submission_df.head(20))

# Save the DataFrame to a CSV file
submission_csv_path_arin = r"/home/aagadre/cse144_final/final_project/ucsc-cse-144-Winter-2024-final-project/test.csv"  # Adjust the path as needed
submission_df.to_csv(submission_csv_path_arin, index=False)

print(f"Submission file saved to {submission_csv_path_arin}")
