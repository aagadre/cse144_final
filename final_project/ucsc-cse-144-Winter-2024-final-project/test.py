import os
import torch
from torchvision import transforms, datasets
from torchvision import models
import torch.optim as optim
from torch.optim import lr_scheduler

# Set device to GPU if available, else CPU
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"Using device: {device}")

print("Is CUDA available:", torch.cuda.is_available())
print("CUDA version:", torch.version.cuda)
print("Number of GPUs:", torch.cuda.device_count())
print("GPU Name:", torch.cuda.get_device_name(0) if torch.cuda.is_available() else "CUDA not available")


train_data_dir = r'C:\Users\Arin Gadre\OneDrive\Documents\CSE144\cse144_final\final_project\ucsc-cse-144-Winter-2024-final-project\train'
test_data_dir = r'C:\Users\Arin Gadre\OneDrive\Documents\CSE144\cse144_final\final_project\ucsc-cse-144-Winter-2024-final-project'

vm_train_dir_arin = r'/home/aagadre/cse144_final/final_project/ucsc-cse-144-Winter-2024-final-project/train'
vm_test_dir_arin = r'/home/aagadre/cse144_final/final_project/ucsc-cse-144-Winter-2024-final-project'

vm_train_dir_edgar = r'/home/eagekyan/cse144_final/final_project/ucsc-cse-144-Winter-2024-final-project/train'
vm_test_dir_edgar = r'/home/eagekyan/cse144_final/final_project/ucsc-cse-144-Winter-2024-final-project'

transform = transforms.Compose([
    transforms.Resize((224, 224)),  # Resize all images to 224x224
    transforms.ToTensor(),  # Convert images to PyTorch tensors
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),  # Normalize images
])

# Load the dataset
train_dataset = datasets.ImageFolder(os.path.join(vm_train_dir_arin, 'train'), transform=transform)
test_dataset = datasets.ImageFolder(os.path.join(vm_test_dir_arin, 'test'), transform=transform)

# Prepare data loaders
train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=16, shuffle=True)
test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=16, shuffle=False)

# Load a pre-trained ResNet50 model
model = models.resnet50(pretrained=True)

# Modify the final layer for your dataset
num_ftrs = model.fc.in_features
model.fc = torch.nn.Linear(num_ftrs, 100)  # Assuming 100 classes in your dataset

# Now move the entire model to the device (GPU or CPU)
#model = model.to(device)

criterion = torch.nn.CrossEntropyLoss()
optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
scheduler = lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)

num_epochs = 10
# Example training loop
for epoch in range(num_epochs):
    print(f'training model in epoch #{epoch+1}')
    model.train()  # Set model to training mode
    running_loss = 0.0
    
    for inputs, labels in train_loader:
        #inputs, labels = inputs.to(device), labels.to(device)  # Move inputs and labels to the correct device
        
        optimizer.zero_grad()
        print(inputs.device)  # Should print cuda:0 if on GPU
        print(model.fc.weight.device)  # Should also print cuda:0 if on GPU

        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()
        
        running_loss += loss.item() * inputs.size(0)
    
    scheduler.step()
    
    epoch_loss = running_loss / len(train_dataset)
    print(f'Epoch {epoch+1}/{num_epochs}, Loss: {epoch_loss:.4f}')
